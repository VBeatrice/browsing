To execute:
---> download Wireshark

1- git clone https://gitlab.com/VBeatrice/browsing

2- cd to local project folder

3- cd browsing

4- close all open browing tabs otherwise they will be closed by program

5- from command line run the following commands separately  (run multiple times for more captures):

    ./open_twitter.py     (starts the capture of twitter traffic)

    ./open_browsing.py    (starts the capture of generic browsing traffic)

    ./open_youtube.py     (starts the capture of youtube traffic) 
