#!/usr/bin/env python3

import webbrowser
import time
import subprocess
import random
import uuid

path = "./Urls/BrowsingUrls.txt"

wifi_interface = input("Please enter wifi interface name: ")
ip_address = input("Please enter your pc IP address in the network you're connected to: ")


with open(path, 'r') as url_file:
    file = url_file.read()
    urls = file.splitlines()
    random.shuffle(urls)
    uid =  str(uuid.uuid4())
    command = "tshark -i " + wifi_interface + " -f 'tcp and dst net " + ip_address + "' -w ./Capture/Browsing/BrowsingCapture"+uid+".pcapng"
    print("Running: " + command)
    try:
        process = subprocess.Popen(command , shell=True)

        for i in [0,20,40,60,80,100]:
            for url in urls[i:i+20]:
                webbrowser.open(url)
                time.sleep(5)
            time.sleep(10)
            subprocess.run("killall $(xdg-mime query default x-scheme-handler/http | sed 's/\.desktop//')", shell=True, check=True)
            time.sleep(1)
        process.terminate()
        process.wait()
    except:
        print("Please make sure that wireshark is installed and that the user you're logged with the permission to use it")
